$(function() {
    var Book = {
        config: {
            a: 'valueA',
            b: 'valueB'
        },
        init: function() {
            $('.book-preview').colorbox({
                width: '75%',
                height: '75%'
            });
        }
    };

    Book.init();

    window.Book = Book;
});