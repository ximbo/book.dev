<?php

return [
    'adminEmail' => 'admin@example.com',
    'uploadDir'  => '@webroot/upload/',
    'uploadWeb'  => '/upload/',
];
