<?php

use yii\db\Schema;
use yii\db\Migration;
use app\traits\DbOptions;
use Faker\Factory;

class m160114_081636_create_authors_table extends Migration
{
    use DbOptions;

    protected $table = 'authors';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'        => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'lastname'  => $this->string()->notNull(),
        ], $this->options());

        $this->seed();
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    /**
     * Сидинг таблицы авторов
     *
     * @param int $limit Количество создаваемых фейковых авторов
     */
    protected function seed($limit = 20)
    {
        $faker = Factory::create('ru_RU');
        $seed = [];
        for ($i = 1; $i <= $limit; $i++) {
            $seed[] = [$faker->firstNameMale, $faker->lastName];
        }

        $this->batchInsert($this->table, ['firstname', 'lastname'], $seed);
    }

}
