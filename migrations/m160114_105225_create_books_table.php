<?php

use yii\db\Schema;
use yii\db\Migration;
use app\traits\DbOptions;

class m160114_105225_create_books_table extends Migration
{
    use DbOptions;

    protected $table = 'books';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'        => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'preview' => $this->string(),
            'date' => $this->date()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
        ], $this->options());

        $this->createIndex('book_name', $this->table, 'name');
        $this->createIndex('book_date', $this->table, 'date');
        $this->addForeignKey('author_book_id_foreign', $this->table, 'author_id', 'authors', 'id', 'CASCADE', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }

}
