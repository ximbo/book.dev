<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JqueryAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('/css/colorbox.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.3/jquery.colorbox.js', ['depends' => JqueryAsset::className()]);
$this->registerJsFile('/js/book.js', ['depends' => JqueryAsset::className()]);
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <br><br>

    <p>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        //'layout'       => "{pager}\n{summary}\n{items}\n{pager}",
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            //'preview',
            //['attribute'=>'preview', 'value' => 'previewImage'],
            [
                'label' => 'Превью',
                'format' => 'raw',
                'value' => function($data){
                    /* @var app\models\BookSearch $data */
                    return Html::a(
                        Html::img($data->thumbnail(), ['alt' => $data->name]),
                        $data->previewImage,
                        ['class' => 'book-preview']
                    );
                },
            ],
            ['attribute' => 'author', 'value' => 'author.fullname'],
            'date',
            'created_at',
            //'author_id',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
