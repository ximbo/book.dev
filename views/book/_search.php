<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Author;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\BookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'fieldConfig' => [
            'template' => '<div>{input}</div><div>{error}</div>',
        ],
    ]); ?>

    <div class="form-group">
        <div class="col-md-8">
            <div class="col-md-6">
                <?= $form->field($model, 'author_id')->dropDownList( ArrayHelper::merge(['' => 'автор'], ArrayHelper::map(Author::find()->all(), 'id', 'fullname')) ) ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'name')->input('text', ['placeholder'=>'название книги']) ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="form-group">
        <div class="col-md-8">
            <div class="col-md-12">
                <label>Дата выхода книги:</label>
                <?= DatePicker::widget(['model' => $model, 'attribute' => 'date_from', 'language' => 'ru', 'dateFormat' => 'yyyy-MM-dd']) ?>
                <label>до</label>
                <?= DatePicker::widget(['model' => $model, 'attribute' => 'date_to', 'language' => 'ru', 'dateFormat' => 'yyyy-MM-dd']) ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <?php ActiveForm::end(); ?>

</div>
