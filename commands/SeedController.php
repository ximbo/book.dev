<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Author;
use app\models\Book;
use Faker\Factory;
use Faker\Generator;
use Carbon\Carbon;
use yii\helpers\Console;

/**
 * Сидинг данных
 *
 * Примеры:
 *
 *      Создать 5 новых авторов:
 *      php yii seed/authors 5
 *
 *      Создать 10 новых книг:
 *      php yii seed/books 10
 *
 *      Создать сразу 7 авторов и книг:
 *      php yii seed 7
 */
class SeedController extends Controller
{
    /* @var Generator */
    protected $faker;

    public function init()
    {
        $this->faker = Factory::create('ru_RU');
    }

    /**
     * Сидинг
     *
     * @param int $limit Количество создаваемых записей
     * @return int
     */
    public function actionIndex($limit = 20)
    {
        $this->actionAuthors($limit);
        $this->actionBooks($limit);
        return static::EXIT_CODE_NORMAL;
    }

    /**
     * Сидинг таблицы авторов
     *
     * @param int $limit Количество создаваемых фейковых авторов
     * @return int
     */
    public function actionAuthors($limit = 20)
    {
        Console::startProgress(0, $limit, "Authors | Seeding");
        for ($i = 1; $i <= $limit; $i++) {
            (new Author([
                'firstname' => $this->faker->firstNameMale,
                'lastname'  => $this->faker->lastName,
            ]))->save();
            Console::updateProgress($i, $limit);
        }
        Console::endProgress();
        echo "Authors | Seeded {$limit} records" . PHP_EOL;
        return static::EXIT_CODE_NORMAL;
    }

    /**
     * Сидинг таблицы книг
     *
     * @param int $limit Количество создаваемых фейковых книг
     * @return int
     */
    public function actionBooks($limit = 20)
    {
        Console::startProgress(0, $limit, "Books | Seeding");
        for ($i = 1; $i <= $limit; $i++) {
            $now = Carbon::now()->toDateTimeString();
            //TODO Вообще тут надо batch-insert и не использовать RAND(), но так лень
            (new Book([
                'name' => $this->faker->realText(rand(20, 80)),
                'date' => $this->faker->dateTimeBetween()->format('Y-m-d'),
                'created_at' => $now,
                'updated_at' => $now,
            ]))->link('author', Author::find()->orderBy('RAND()')->one());
            Console::updateProgress($i, $limit);
        }
        Console::endProgress();
        echo "Books | Seeded {$limit} records" . PHP_EOL;
        return static::EXIT_CODE_NORMAL;
    }
}
