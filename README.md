Тестовое задание
============================

Сделать на Yii2 возможность только зарегистрированным пользователям просматривать, удалять, редактировать записи в таблице "books"

        id,
        name,
        date_create,   / дата создания записи
        date_update,   / дата обновления записи
        preview,       / путь к картинке превью книги
        date,          / дата выхода книги
        author_id      / ид автора в таблице авторы

Таблица "authors" редактирование таблицы авторов не нужно, необходимо ее просто заполнить тестовыми данными.

        id,
        firstname,   / имя автора
        lastname,    / фамилия автора

В итоге страница управления книгами должна выглядеть так: 

[![Страница управления книгами](http://dl.dropbox.com/u/14927161/Selection_214.png)](http://dl.dropbox.com/u/14927161/Selection_214.png)

[http://dl.dropbox.com/u/14927161/Selection_214.png](http://dl.dropbox.com/u/14927161/Selection_214.png)

###Краткая инструкция

####Локальное развертывание

После клонирования ```cp VagrantConfig.yml.example VagrantConfig.yml```

Внести необходимые изменения в настройки виртуальной машины в файле VagrantConfig.yml

####Сидинг

После установки применить миграции ```php yii migrate```, выполнить ```php yii seed```. Это создаст 20 тестовых авторов и книг.

```php yii seed/authors 50``` - создать 50 авторов

```php yii seed/books 15``` - создать 15 книг, привязанных к случайным существующим на данный момент авторам

