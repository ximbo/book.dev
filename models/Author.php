<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;
use \yii\db\ActiveQuery;

/**
 * Модель автора книги
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 *
 * @property Book[] $books Все книги данного автора
 */
class Author extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['firstname', 'lastname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'firstname' => 'Имя',
            'lastname'  => 'Фамилия',
            'fullname'  => 'Автор',
        ];
    }

    /**
     * Полное имя автора
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->lastname . ' ' . $this->firstname;
    }

    /**
     * Все книги данного автора
     *
     * @return ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['author_id' => 'id']);
    }
}
