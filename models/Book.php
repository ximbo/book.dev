<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;
use \yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\services\Thumb;

/**
 * Модель книги
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $name
 * @property string $preview
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $previewImage
 * @property Author $author Автор книги
 */
class Book extends ActiveRecord
{
    /* @var string Дефолтное превью для книг */
    protected $defaultPreview = '/img/empty.png';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value'              => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'name', 'date'], 'required'],
            [['author_id'], 'integer'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['name', 'preview'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'author_id'    => 'Автор',
            'name'         => 'Название',
            'preview'      => 'Превью',
            'date'         => 'Дата выхода книги',
            'created_at'   => 'Дата добавления',
            'updated_at'   => 'Обновлено',

            'author'       => 'Автор',
            'previewImage' => 'Превью',
        ];
    }

    /**
     * Автор книги
     *
     * @return ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    /**
     * Получение превьюшки книги. Если не задано - возвращаем дефолтный
     * Будет доступно в виде публичного свойства $model->previewImage
     *
     * @return string
     */
    public function getPreviewImage()
    {
        return $this->isPreviewExists() ? $this->preview : $this->defaultPreview;
    }

    /**
     * Абсолютный путь к файлу превью
     *
     * @return string
     */
    public function absolutePreviewPath()
    {
        return (!empty($this->preview)) ? (string) Yii::getAlias('@webroot' . $this->preview) : '';
    }

    /**
     * Проверка существования файла превью
     *
     * @return bool
     */
    public function isPreviewExists()
    {
        return file_exists($this->absolutePreviewPath());
    }

    /**
     * Получение тумба
     *
     * @param int $width
     * @param int $height
     * @return string
     */
    public function thumbnail($width = 100, $height = 100)
    {
        return Thumb::url($this->getPreviewImage(), $width, $height);
    }
}
