<?php

namespace app\services;

use Yii;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * Class Thumb
 * @package app\services
 *
 * Сервис для работы с изображениями
 */
class Thumb
{
    /* @var string Оригинальный файл изображения (вида '/upload/some.jpg', т.е. относительно вебрута) */
    protected $filename;

    /* @var string Абсолютный путь к директории тумбов */
    protected $thumbDir;

    /* @var string Веб-путь к директории тумбов */
    protected $thumbUrl;

    /* @var int */
    protected $width = 100;

    /* @var int */
    protected $height = 100;

    /**
     * @param string $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @param int $width
     * @return Thumb
     */
    public function setWidth($width = 100)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @param int $height
     * @return Thumb
     */
    public function setHeight($height = 100)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @param int $width
     * @param int $height
     * @return Thumb
     */
    public function setBox($width = 100, $height = 100)
    {
        return $this->setWidth($width)->setHeight($height);
    }

    /**
     * @param $thumbDir
     * @return Thumb
     */
    public function setThumbDir($thumbDir)
    {
        $this->thumbDir = $thumbDir;
        return $this;
    }

    /**
     * @param $thumbUrl
     * @return Thumb
     */
    public function setThumbUrl($thumbUrl)
    {
        $this->thumbUrl = $thumbUrl;
        return $this;
    }

    /**
     * Получение абсолютного пути файла изображения
     *
     * @return string
     */
    public function absolute()
    {
        return Yii::getAlias('@webroot') . $this->filename;
    }

    /**
     * Если такой директории нет - создаем
     *
     * @return bool
     */
    public function createDir()
    {
        $dir = $this->thumbDir . $this->mask();
        return FileHelper::createDirectory($dir);
    }

    /**
     * Создание тумба и получение его абсолютного пути
     *
     * @return string
     */
    public function createThumbnail()
    {
        $this->createDir();
        $thumbFile = $this->thumbDir . $this->mask(basename($this->filename));
        Image::thumbnail($this->absolute(), $this->width, $this->height)->save($thumbFile);
        return $thumbFile;
    }

    /**
     * Маска тумба
     * Вернет маску директории под тумбы указанного размера, если не задано имя файла
     *
     * @param string $name Имя файла
     * @return string
     */
    public function mask($name = '')
    {
        return $this->width . 'x' . $this->height . '/' . $name;
    }

    /**
     * Получение веб-адреса тумба
     *
     * @return string
     */
    public function getThumbnail()
    {
        $filename = basename($this->filename);
        if (!file_exists($this->thumbDir . $this->mask($filename))) {
            $this->createThumbnail();
        }
        return $this->thumbUrl . $this->mask($filename);
    }

    /**
     * Создание с дефолтными параметрами
     *
     * @params string $filename
     * @return Thumb
     */
    public static function create($filename)
    {
        $thumbs = 'thumbs/';
        $thumbDir = Yii::getAlias(Yii::$app->params['uploadDir']) . $thumbs;
        $thumbUrl = Yii::getAlias(Yii::$app->params['uploadWeb']) . $thumbs;
        return (new static($filename))->setThumbDir($thumbDir)->setThumbUrl($thumbUrl);
    }

    /**
     * Получение веб-адреса тумба указанного изображения с заданными размерами
     *
     * @param string $filename
     * @param int $width
     * @param int $height
     * @return string
     */
    public static function url($filename, $width = 100, $height = 100)
    {
        return static::create($filename)->setBox($width, $height)->getThumbnail();
    }

}
