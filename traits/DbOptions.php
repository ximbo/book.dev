<?php

namespace app\traits;

use yii\db\Migration;

trait DbOptions
{
    /**
     * Для mysql принудительно устанавливает кодировку и движок для таблиц
     *
     * @return null|string
     */
    protected function options()
    {
        /* @var Migration $this */
        return ($this->db->driverName === 'mysql') ? 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB' : null;
    }
}
