upstream backend-book {server unix:/var/run/php5-book.sock;}

server {
    charset utf-8;
    client_max_body_size 128M;

    listen 80;

    server_name {{ nginx.servername }};
    root {{ nginx.docroot }};
    access_log /var/www/book/logs/nginx.access.log;
    error_log /var/www/book/logs/nginx.error.log;
    index index.php;

    location / {
        # Redirect everything that isn't a real file to index.php
        try_files $uri $uri/ /index.php?$args;
    }

    location ~* ^.+\.(jpg|jpeg|jpe|gif|css|png|js|ico|bmp|woff|pdf|mov|fla|zip|rar,txt)$ {
        access_log off;
        expires 10d;
        break;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root/$fastcgi_script_name;
        fastcgi_pass backend-book;
        try_files $uri =404;
    }

    location ~ /\. {
        deny all; #deny any .git, .htaccess, .svn, etc
    }
}
