[book]

listen = /var/run/php5-book.sock
listen.mode = 0666
user = vagrant
group = vagrant
chdir = /var/www/book

access.log = /var/www/book/logs/fpm.access.log
php_admin_value[error_log] = /var/www/book/logs/fpm.error.log
php_admin_flag[log_errors] = on
php_admin_value[upload_tmp_dir] = /var/www/book/tmp
php_admin_value[soap.wsdl_cache_dir] = /var/www/book/tmp
php_admin_value[upload_max_filesize] = 100M
php_admin_value[post_max_size] = 100M
php_admin_value[open_basedir] = /var/www/book/data/:/tmp/:/var/www/book/tmp/
;php_admin_value[disable_functions] = exec,passthru,shell_exec,system,proc_open,popen,curl_multi_exec,parse_ini_file,show_source,highlight_file,com_load_typelib
php_admin_value[cgi.fix_pathinfo] = 0
php_admin_value[date.timezone] = {{ server.timezone }}
php_admin_value[apc.cache_by_default] = 0

pm = dynamic
pm.max_children = 10
pm.start_servers = 2
pm.min_spare_servers = 2
pm.max_spare_servers = 4
